import { Component, ElementRef, OnInit, ViewChild, viewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import * as THREE from 'three';
import Stats from 'stats.js'
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {


  @ViewChild('three', { static: true }) threeRef!: ElementRef;
  @ViewChild('stats', { static: true }) statsRef!: ElementRef;

  ngOnInit(): void {
    console.log('查看当前屏幕设备像素比', window.devicePixelRatio);

    this.init();

  }


  init = () => {

    const obj = {
      color: 0x00ffff,
      scale: 0,
      bool: false,
    };

    const stats = new Stats();
    const scene = new THREE.Scene();
    const axesHelper = new THREE.AxesHelper(150);
    scene.add(axesHelper);


    //BoxGeometry：长方体
    const geometry = new THREE.BoxGeometry(20, 20, 20);
    // SphereGeometry：球体
    // const geometry = new THREE.SphereGeometry(50);
    // CylinderGeometry：圆柱
    // const geometry = new THREE.CylinderGeometry(50,50,100);
    // PlaneGeometry：矩形平面
    // const geometry = new THREE.PlaneGeometry(100,50);
    // CircleGeometry：圆形平面
    // const geometry = new THREE.CircleGeometry(50);

    // const geometry = new THREE.BoxGeometry(100, 100, 100); // 长方体
    // const material = new THREE.MeshLambertMaterial({
    //     color: 0xff0000,  // 材质为红色
    //     transparent: true,//开启透明
    //     opacity: 0.9,//设置透明度
    //     side: THREE.DoubleSide
    // })

    const material = new THREE.MeshPhongMaterial({
      color: 0xff0000,
      shininess: 20, // 高光部分的亮度， 默认30
      specular: 0x444444, //高光部分的颜色
    })


    const mesh = new THREE.Mesh(geometry, material); // 网格模型
    mesh.castShadow = true;
    mesh.position.set(0, 10, 0); // mesh在scene中的位置
    scene.add(mesh);


    const planeGeo = new THREE.PlaneGeometry(400, 400);
    const planeMesh = new THREE.Mesh(
      planeGeo,
      new THREE.MeshLambertMaterial({ color: 0xeeeeee }),
    );
    planeMesh.receiveShadow = true;
    planeMesh.rotation.x = -Math.PI / 2;
    planeMesh.position.set(0, 0, 0);
    scene.add(planeMesh);

    //添加聚光灯，该灯光类型能够产生阴影
    var spotLight = new THREE.SpotLight(0xffffff, 10)
    spotLight.position.set(100, 200, 100)
    //开启该聚光灯的投影效果
    spotLight.castShadow = true
    //设置该灯光的阴影质量
    spotLight.shadow.mapSize.width = 1024
    spotLight.shadow.mapSize.height = 1024
    spotLight.decay = 0.0;//设置光源不随距离衰减
    spotLight.shadow.mapSize.set(2024, 2024);
    spotLight.shadow.radius = 3
    spotLight.angle = Math.PI / 8;//光锥角度的二分之一
    //场景添加该灯光
    scene.add(spotLight)

    scene.add(
      new THREE.SpotLightHelper(spotLight) // 光源辅助
    )

    // 【点光源】
    const pointLight = new THREE.PointLight(0xffffff, 3.0);
    pointLight.decay = 0.0;//设置光源不随距离衰减
    pointLight.position.set(200, 300, 150);//点光源位置
    // scene.add(pointLight);

    const pointLightHelper = new THREE.PointLightHelper(pointLight, 10);// 光源辅助观察
    scene.add(pointLightHelper);

    // 【环境光】:没有特定方向，整体改变场景的光照明暗
    const ambient = new THREE.AmbientLight(0xffffff, .5);
    scene.add(ambient);

    // 【平行光】
    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(80, 100, 100); // 设置光源的方向
    // directionalLight.target = mesh; // 光源指向的对象
    // scene.add(directionalLight);

    const dirLightHelper = new THREE.DirectionalLightHelper(directionalLight, 5, 0xff0000);
    scene.add(dirLightHelper);

    // 相机输出的画布尺寸
    const width = window.innerWidth;
    const height = window.innerHeight;
    const camera = new THREE.PerspectiveCamera(80, width / height, 1, 3000); // 实例一个相机
    camera.position.set(200, 200, 200); // 设置相机位置
    camera.lookAt(0, 0, 0); // 设置相机方向为坐标
    // camera.lookAt(mesh.position); // 设置相机方向为mesh位置

    const renderer = new THREE.WebGLRenderer({
      antialias: true // 设置渲染器锯齿属性
    }); // 实例一个渲染器
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap
    renderer.setSize(width, height); // 设置渲染器尺寸
    renderer.setPixelRatio(window.devicePixelRatio); // 设置像素比为设备像素比
    renderer.setClearColor(0x444444, 1); //设置背景颜色

    const clock = new THREE.Clock();
    const render = () => {
      stats.update();
      renderer.render(scene, camera); // 渲染
      if (obj.bool) {
        mesh.rotateY(0.002);
      }

      requestAnimationFrame(render);
    }
    render();
    this.threeRef.nativeElement.appendChild(renderer.domElement);
    this.statsRef.nativeElement.appendChild(stats.dom);

    // 设置相机控件轨道控制器OrbitControls
    new OrbitControls(camera, renderer.domElement);
    // const controls = new OrbitControls(camera, renderer.domElement);
    // 如果OrbitControls改变了相机参数，重新调用渲染器渲染三维场景
    // controls.addEventListener('change', function () {
    //     // renderer.render(scene, camera); //执行渲染操作
    // });//监听鼠标、键盘事件

    // canvas画布宽度高动态变化
    window.onresize = () => {
      // 重置渲染器输出画布canvas尺寸
      renderer.setSize(window.innerWidth, window.innerHeight);
      // 全屏情况下：设置观察范围长宽比aspect为窗口宽高比
      camera.aspect = window.innerWidth / window.innerHeight;
      // 渲染器执行render方法的时候会读取相机对象的投影矩阵属性projectionMatrix
      // 但是不会每渲染一帧，就通过相机的属性计算投影矩阵(节约计算资源)
      // 如果相机的一些属性发生了变化，需要执行updateProjectionMatrix ()方法更新相机的投影矩阵
      camera.updateProjectionMatrix();
    }


    const gui = new dat.GUI();

    const matFolder = gui.addFolder('材质');
    const lightFolder = gui.addFolder('光线');
    //改变交互界面style属性
    gui.domElement.style.right = '0px';
    gui.domElement.style.width = '300px';



    // gui增加交互界面，用来改变obj对应属性
    matFolder.add(ambient, 'intensity', 0, 2.0).name('环境光强度');
    matFolder.add(directionalLight, 'intensity', 0, 2.0).name('平行光强度').step(0.1);
    // .addColor()生成颜色值改变的交互界面
    matFolder.addColor(obj, 'color').onChange(function (value) {
      mesh.material.color.set(value);
    });
    lightFolder.add(directionalLight.position, 'x', -400, 400);
    lightFolder.add(directionalLight.position, 'y', -400, 400);
    lightFolder.add(directionalLight.position, 'z', -400, 400);

    gui.add(mesh.position, 'x', 0, 180).name('X坐标');
    gui.add(mesh.position, 'y', [-100, 0, 100]).name('Y坐标');
    gui.add(obj, 'scale', { left: -100, "居中": 0, right: 100 })
      .name('Z坐标')
      .onChange(value => {
        mesh.position.z = value;
      });



    gui.add(obj, 'bool').name('是否旋转');


  }
}
